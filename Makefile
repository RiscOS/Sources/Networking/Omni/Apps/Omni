# Copyright 2000 Pace Micro Technology plc
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Makefile for Omni (the module)
#

COMPONENT   = Omni
override TARGET = OmniClient
OBJS        = Client Close CoreInit Events ExtLib ExtLibAsm Filer FileType \
              Heap List MakeError Mount Omni Parse Print PrintFS Redraw \
              Sort Time _Args _Close _Errs _File _Func _GBPB _GetBytes \
              _Open _PutBytes _Veneers
CINCLUDES   = ${TCPIPINC}
HDRS        =
ASMHDRS     = OmniClient
ASMCHDRS    = OmniClient
CMHGAUTOHDR = OmniClient
CMHGFILE    = OmniClient
CMHGFILE_SWIPREFIX = Omni
CMHGDEPENDS = _Args _Close _GBPB _PutBytes Filer
CFLAGS      = ${C_NOWARN_NON_ANSI_INCLUDES} ${C_STKCHK}
CDEFINES    = ${EXPERT} -DNDEBUG -DINET
CDFLAGS     = -DDEBUG -DDEBUGLIB
LIBS        = ${DESKLIB} ${INETLIB} ${ASMUTILS}
CUSTOMRES   = no
CUSTOMSA    = custom

include CModule

# Installing the disc !Omni application
install_custom: ${SA_TARGET_RULE}
	@${MAKE} install -f app${EXT}mk COMPONENT=Omni INSTDIR=${INSTDIR} OMNIMOD=${SA_TARGET_RULE}

standalone_custom: ${SA_TARGET_RULE}
	@${ECHO} ${COMPONENT}: ram module built

# Dynamic dependencies:
