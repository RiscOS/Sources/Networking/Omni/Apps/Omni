# Copyright 2022 RISC OS Open Limited
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Makefile for Omni (the container application)
#

INSTTYPE        = app
INSTAPP_FILES   = !Run !Boot !Help Messages Templates Template3D ${OMNIMOD}:RMStore \
                  HourOff:Utils HourOn:Utils \
                  Dormant:Utils.Access KillTask:Utils.Access \
                  Files.Extensions:Files Files.StartShare:Files Files.Startup:Files \
                  !Sprites:Themes !Sprites22:Themes !Sprites11:Themes \
                  LanMan:Themes Net:Themes NFS:Themes Share:Themes \
                  Morris4.!Sprites:Themes.Morris4 Morris4.!Sprites22:Themes.Morris4 \
                  Morris4.LanMan:Themes.Morris4 Morris4.Net:Themes.Morris4 Morris4.NFS:Themes.Morris4 Morris4.Share:Themes.Morris4 \
                  Ursula.!Sprites:Themes.Ursula Ursula.!Sprites22:Themes.Ursula \
                  Ursula.LanMan:Themes.Ursula Ursula.Net:Themes.Ursula Ursula.NFS:Themes.Ursula Ursula.Share:Themes.Ursula
INSTAPP_VERSION = Messages

include CApp

# Dynamic dependencies:
