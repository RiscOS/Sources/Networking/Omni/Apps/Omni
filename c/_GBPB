/* Copyright 2000 Pace Micro Technology plc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
 * Program:	_GBPB.c - core application routines
 *
 * Project:	!Omni OmniClient(tm) project
 *
 * Author:	Nick Smith
 *              ANT Limited
 *              Cambridge
 *              Internet: nas@ant.co.uk
 *
 * Date:	23 December 1994
 * Last Edited:	23 December 1994
 *
 * Copyright 1994 by ANT Limited
 */

#include <kernel.h>
#include <swis.h>

#include <DebugLib/DebugLib.h>

#include "Filer.h"
#include "OmniClient.h"
#include "Print.h"
#include "_Errs.h"
#include "_Veneers.h"


/*  Support for OS_GBPB calls.
 */
extern _kernel_oserror *fsentry_gbpb(FSEntry_GBPB_Parameter *parm)
{
  _kernel_oserror *err = NULL;
  print_record *pr = NULL;

  dprintf (("", "fsentry_gbpb: %x\n", parm->reason));
  switch (parm->reason)
  {
    case FSEntry_GBPB_Reason_PutMultipleBytes:
    {
      int  bytes_remaining;

      /* Check for a valid print_id word */
      dprintf (("", "fsentry_gbpb: put multiple\n"));
      pr = PrintFind(parm->handle, -1, -1, -1);
      if (pr == NULL)
      {
        return(&mb_nfserr_BadParameters);
      }

      /* Send data with SWI Omni_SendJob */
      err = _swix (Omni_SendJob, _INR (0, 2) | _OUT (2),
                   pr->print_id, parm->address, parm->number,
                   &bytes_remaining);
      if (!err)
      {
        parm->address = parm->address + (parm->number - bytes_remaining);
        parm->number = bytes_remaining;
      }
      break;
    }

    default:
      err = &mb_nfserr_DummyFSDoesNothing;
      dprintf (("", "fsentry_gbpb: not supported\n"));
      break;
  }
  return(err);
}
